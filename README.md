# Percona XtraDB Cluster using Ansible

## Requirements
- 3 Debian-based servers with root account accessible through SSH.

### Add SSH keys to servers

```bash
ansible-playbook -i hosts.ini pxc-add-ssh-key-playbook.yml --ask-pass
``` 

### Edit hosts.ini
- Change IP addresses to match your infrastructure.

## Create PXC cluster automatically
- Initiate SSH agent, delete old cluster and create new.
```bash
ssh-add
ansible-playbook -i hosts.ini pxc-all-plays.yml --ask-become-pass
``` 
## Manage PXC cluster "manually"

0) Initiate SSH agent
```bash
ssh-add
``` 
1) Destroy PXC cluster
```bash
ansible-playbook -i hosts.ini pxc-delete-play.yml --ask-become-pass
``` 
2) Install PXC cluster
```bash
ansible-playbook -i hosts.ini pxc-install-play.yml --ask-become-pass
``` 
3) Setup PXC cluster
```bash
ansible-playbook -i hosts.ini pxc-setup-play.yml --ask-become-pass
``` 
4) Shutdown servers
```bash
ansible-playbook -i hosts.ini pxc-shutdown-play.yml --ask-become-pass
``` 

## Check functionality in database
- Access random server using SSH.
- \# mysql -u root -p
  - password: click enter
- mysql> show status like 'wsrep_cluster%';
- mysql> show status like 'wsrep_%';

```sql
+----------------------------+--------------------------------------+
| Variable_name              | Value                                |
+----------------------------+--------------------------------------+
| wsrep_cluster_weight       | 3                                    |
| wsrep_cluster_capabilities |                                      |
| wsrep_cluster_conf_id      | 4                                    |
| wsrep_cluster_size         | 3                                    |
| wsrep_cluster_state_uuid   | b788d859-9272-11ed-8ed1-b377bf971f35 |
| wsrep_cluster_status       | Primary                              |
+----------------------------+--------------------------------------+
```
## Check functionality in log
- Access random server using SSH
- less +G /var/log/mysql/error.log

```bash
[Galera] Receiving IST...100.0% (2/2 events) complete.
[Galera] ================================================
View:
  id: b788d859-9272-11ed-8ed1-b377bf971f35:5
  status: primary
  protocol_version: 4
  capabilities: MULTI-MASTER, CERTIFICATION, PARALLEL_APPLYING, REPLAY, ISOLATION, PAUSE, CAUSAL_READ, INCREMENTAL_WS, UNORDERED, PREORDERED, STREAMING, NBO
  final: no
  own_index: 2
  members(3):
        0: cd03f67b-9272-11ed-aafa-b7164df6f5f5, mysql-2
        1: cd37e601-9272-11ed-a2c2-8e81e94b8acb, mysql-3
        2: fe143259-9272-11ed-99d6-233230c3582f, mysql-1
=================================================
[WSREP] Server status change initialized -> joined
[WSREP] wsrep_notify_cmd is not defined, skipping notification.
[WSREP] wsrep_notify_cmd is not defined, skipping notification.
[Galera] Draining apply monitors after IST up to 5
[Galera] IST received: b788d859-9272-11ed-8ed1-b377bf971f35:5
[Galera] Recording CC from sst: 5
[Galera] Lowest cert index boundary for CC from sst: 5
[Galera] Min available from gcache for CC from sst: 1
[Galera] 2.0 (mysql-1): State transfer from 1.0 (mysql-3) complete.
[Galera] SST leaving flow control
[Galera] Shifting JOINER -> JOINED (TO: 5)
[Galera] Processing event queue:... -nan% (0/0 events) complete.
[Galera] Member 2.0 (mysql-1) synced with group.
[Galera] Processing event queue:...100.0% (1/1 events) complete.
[Galera] Shifting JOINED -> SYNCED (TO: 5)
[Galera] Server mysql-1 synced with group
[WSREP] Server status change joined -> synced
[WSREP] Synchronized with group, ready for connections
[WSREP] wsrep_notify_cmd is not defined, skipping notification.
[Galera] (fe143259-99d6, 'tcp://0.0.0.0:4567') turning message relay requesting off
[Galera] 1.0 (mysql-3): State transfer to 2.0 (mysql-1) complete.
[Galera] Member 1.0 (mysql-3) synced with group.
```

## PXC Documentation
- [install-from-repository](https://docs.percona.com/percona-xtradb-cluster/8.0/install/apt.html#installing-from-repository)
- [configure](https://docs.percona.com/percona-xtradb-cluster/8.0/configure.html#configure)
- [bootstrap node #1](https://docs.percona.com/percona-xtradb-cluster/8.0/bootstrap.html#bootstrap)
- [add nodes #2, #3](https://docs.percona.com/percona-xtradb-cluster/8.0/add-node.html#add-node)
